@extends("layout.main")

@section('judul')
Biodata
@endsection

@section('content')

<h1>{{$cast -> nama}}</h1>
<p>{{$cast -> bio}} </p>

<a href="/cast" class="btn btn-primary btn-sm mt-3">Back</a>
@endsection