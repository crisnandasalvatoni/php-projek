@extends("layout.main")

@section('judul')
Current Data
@endsection

@section('content')

<table class="table table-striped">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
 <tbody>
 @forelse ($cast as $nilai => $value)
    <tr>
    <td>{{$nilai + 1}}</td>
    <td>{{$value -> nama}}</td>
    <td>{{$value -> umur}}</td>
    <td>
    <form action="/cast/{{$value -> id}}" method="POST">
        @csrf
        @method('DELETE')
        <a href="/cast/{{$value->id}}" class="btn btn-secondary btn-sm mr-5">Selengkapnya</a>
    <a href="/cast/{{$value->id}}/edit" class="btn btn-success btn-sm mr-5">Edit</a>
        <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
    </form>
     </td>
    </tr>
@empty
    <td>No Data</td>
@endforelse
 </tbody>
</table>

<a href="/cast/create" class="btn btn-primary btn-sm mt-4">Add Cast</a>
@endsection