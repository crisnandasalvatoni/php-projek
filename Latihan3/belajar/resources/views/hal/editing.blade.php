@extends("layout.main")

@section('judul')
Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="mb-3">
    <label class="form-label">Nama Aktor</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" >
    
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-outline">
    <label class="form-label" for="typeNumber">Umur</label>
    <input type="number" name="umur" value="{{$cast->umur}}" class="form-control" />
    
</div>
@error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="mb-3">
    <label class="form-label">Bio</label>
   <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary mt-2">Submit</button>
</form>
<a href="/cast" class="btn btn-primary btn-sm mt-2">Back</a>
@endsection