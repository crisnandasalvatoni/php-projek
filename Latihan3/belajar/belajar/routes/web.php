<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BioController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [IndexController::class, 'dashboard']);
Route::get('/regist', [BioController::class, 'regist']);
Route::post('/posting', [BioController::class, 'posting']);

Route::get('/', function(){
    return view('layout.main');
});
Route::get('/data-table', function(){
    return view('layout.data');
});
Route::get('/table', function(){
    return view('layout.table');
});