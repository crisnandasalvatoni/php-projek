<?php 

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo "Name:  $sheep->name <br>"; // "shaun"
echo "Legs: $sheep->legs  <br>"; // 4
echo "cold blooded: $sheep->cold_blooded"; // "no"
echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Name:  $sungokong->name <br>";
echo "Legs: $sungokong->legs  <br>";
echo "cold blooded: $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"

echo "<br>";
$kodok = new Frog("buduk");

echo "Name:  $kodok->name <br>";
echo "Legs: $kodok->legs  <br>"; 
echo "cold blooded: $kodok->cold_blooded <br>"; 
$kodok->jump() ; // "hop hop"