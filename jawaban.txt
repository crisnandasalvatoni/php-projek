NO 1
CREATE DATABASE myshop;


NO 2
CREATE TABLE USERS(
    id	int PRIMARY KEY AUTO_INCREMENT,
name	varchar(255),
email	varchar(255),
password	varchar(255)
);

CREATE TABLE CATEGORIES(
    id	int PRIMARY KEY AUTO_INCREMENT,
name	varchar(255),
);


CREATE TABLE ITEMS(
    id	int PRIMARY KEY AUTO_INCREMENT,
name	varchar(255),
description	varchar(255),
price	integer,
stock	integer,
category_id	integer,
    FOREIGN KEY(category_id) REFERENCES categories(id));

NO 3
INSERT INTO users(name,email,password)
 VALUES ("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

 INSERT INTO categories(name)VALUES("gadget"),(
"cloth"),
("men"),
("women")
,("branded");

INSERT INTO items(name,description,price,stock,category_id)
 VALUES ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
 ("Uniklooh","baju keren dari brand ternama",500000,50,2),
 ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

NO 4
 a
 SELECT id,name,password FROM users;
 b
 SELECT * FROM items WHERE price > 1000000;
 SELECT * from items WHERE name LIKE "%watch";
 c
 SELECT items.id, items.name, items.description, items.price,
 items.stock,items.category_id, categories.name AS categories
  FROM items LEFT JOIN categories on items.category_id = categories.id;

NO 5
UPDATE items set price=2500000 WHERE id = 1

